import {UserController} from "./Controller/Users";

export class WebMain {
    static App() {
        const searchBtn = document.getElementById("findButton");
        searchBtn.addEventListener("click", (e) => {
            WebMain.onSearchButtonClick(e);
        }, false);
    }

    static onSearchButtonClick(event) {
        let username = document.getElementById("username").value;

        event.preventDefault();
        event.stopPropagation();
        UserController.getUserData(username);
    }
}
