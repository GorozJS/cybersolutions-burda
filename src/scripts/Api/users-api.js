import {polyfill} from 'es6-promise';
polyfill(); // <-- For IE -_-

export class UsersApi {
    fetchUsers(username) {
        let usersEndpoint = `https://gitlab.com/api/v4/users/?search=${username}`;

        return new Promise(function (resolve, reject) {
            let oXRH = new XMLHttpRequest();
            oXRH.open('GET', usersEndpoint);
            oXRH.setRequestHeader('PRIVATE-TOKEN', 'Pt31_tuLW-rNo6k1272-');
            oXRH.onload = function () {
                if (this.status >= 200 && this.status < 300) {
                    resolve(oXRH.response);
                } else {
                    reject({
                        status: this.status,
                        statusText: oXRH.statusText
                    });
                }
            };
            oXRH.onerror = function () {
                reject({
                    status: this.status,
                    statusText: oXRH.statusText
                });
            };

            oXRH.send();
        });
    }
}
