import {UsersApi} from "../Api/users-api";

export class UserController {
    static renderList(data) {
        let items = "";

        data.forEach(item => {
            items +=
                `<a class="card" href=${item.web_url} target="_blank">
                    <img class="card__picture" src=${item.avatar_url} alt="">
                    <div class="card__body">
                    <div class="card__name">${item.name}</div>
                    <div class="card__username">@${item.username}</div>
                    </div>
                </a>`;
        });

        document.getElementById("root").innerHTML = items;
    }

    static getUserData(name) {
        let users = new UsersApi();

        users.fetchUsers(name)
            .then(data => {
                UserController.renderList(JSON.parse(data));
            })
            .catch(err => {
                console.error('Oops', err.status);
            });
    }
}
